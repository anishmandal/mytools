from django.db import models

# Create your models here.


class MrAudioData(models.Model):
    title = models.CharField(max_length=20)
    author = models.CharField(max_length=20)
    description = models.TextField(blank=True, null=True)
    user_id = models.BigIntegerField(default=1)
    location = models.FileField()
    isActive = models.BooleanField(default=True)

    def __str__(self):
        return self.title


class AudioMeta(models.Model):
    audioData_id = models.ForeignKey(MrAudioData, on_delete=models.CASCADE)
    lastPlayed = models.DateTimeField(auto_now_add=True)
    playedTime = models.IntegerField()
    user_id = models.BigIntegerField(default=1)
