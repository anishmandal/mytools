from django.http import Http404
from django.shortcuts import render, get_object_or_404


# Create your views here.
def main_page(request):
    title = "Mr Audio"
    return render(request, "mraudio\main_page.html", {"title": title})
