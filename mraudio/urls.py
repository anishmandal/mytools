from django.urls import path, re_path
from .views import main_page
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', main_page),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_URL)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_URL)
