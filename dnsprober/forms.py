from django import forms


class DNSForm(forms.Form):
    dns_box = forms.CharField()
    request_ip = forms.CharField()
