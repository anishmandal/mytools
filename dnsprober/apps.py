from django.apps import AppConfig


class DnsproberConfig(AppConfig):
    name = 'dnsprober'
