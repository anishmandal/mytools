from django.urls import path, re_path
from .views import dns_page


urlpatterns = [
    path('', dns_page),
]
