from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from .forms import DNSForm

# Create your views here.


def dns_page(request):
    form = DNSForm(request.POST or None)
    setPingResponce = ''
    if form.is_valid():
        dns_box = form.cleaned_data['dns_box']
        request_ip = form.cleaned_data['request_ip']
        setPingResponce = dns_function(dns_box)
    template_name = "dns_page.html"
    obj = []
    context = {
        "obj": obj,
        "setPingResponce": setPingResponce
    }
    return render(request, template_name, context)


def dns_function(dns_box):
    import dns.resolver
    query = dns_box
    answers = dns.resolver.query(query)
    print(answers.response.answer)
    return False
