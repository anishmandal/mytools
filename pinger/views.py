from django.http import Http404
from django.shortcuts import render, get_object_or_404
from .models import PingerData
from .forms import PingForm
# Create your views here.


def pinger_page(request):
    form = PingForm(request.POST or None)
    setPingResponce = ''
    if form.is_valid():
        print(form.cleaned_data)
        ping_box = form.cleaned_data['ping_box']
        request_ip = form.cleaned_data['request_ip']
        # objSave = PingerData.objects.create(
        #     ping_box=ping_box,
        #     request_ip=request_ip
        # )
        # objSave.save()
        setPingResponce = ping_function(ping_box)
    template_name = "pinger_page.html"
    obj = []

    context = {
        "obj": obj,
        # "form": form
        "setPingResponce": setPingResponce
    }
    return render(request, template_name, context)


def pinger_post(request):
    iddata = 1
    form = PingForm(request.POST or None)
    if form.is_valid():
        objSave = PingerData.objects.create(**form.cleaned_data)
    template_name = "pinger_page.html"
    obj = get_object_or_404(PingerData, id=iddata)
    context = {
        "obj": obj,
    }
    return render(request, template_name, context)


def ping_function(ping_box):
    import os
    data = os.popen("ping " + ping_box).read()
    return data
