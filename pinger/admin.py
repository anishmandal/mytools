from django.contrib import admin

# Register your models here.

from .models import PingerData

admin.site.register(PingerData)
