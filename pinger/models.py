from django.db import models

# Create your models here.


class PingerData(models.Model):
    ping_box = models.TextField()
    request_ip = models.TextField()
