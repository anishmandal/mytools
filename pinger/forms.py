from django import forms


class PingForm(forms.Form):
    ping_box = forms.CharField()
    request_ip = forms.CharField()
