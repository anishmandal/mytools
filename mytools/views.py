from django.http import HttpResponse
from django.shortcuts import render


def home_page(request):
    title = "Home"
    return render(request, "main_page.html", {"title": title})


def about_page(request):
    title = "About"
    return render(request, "about_page.html", {"title": title})


def pinger_page(request):
    title = "Pinger"
    return render(request, "pinger_page.html", {"title": title})


def main_page(request):
    return HttpResponse("Main Page")
